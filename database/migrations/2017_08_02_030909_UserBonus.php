<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserBonus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bonus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->integer('coin')->unsigned()->nullable()->default(0);
            $table->integer('click')->unsigned()->nullable()->default(0);
            $table->float('rate', 20, 15)->nullable()->default(0);
            $table->integer('before_bonus_wallet')->unsigned();
            $table->integer('before_bonus_click')->unsigned();
            $table->integer('before_bonus_coin')->unsigned();
            $table->integer('after_bonus_wallet')->unsigned();
            $table->integer('after_bonus_click')->unsigned();
            $table->integer('after_bonus_coin')->unsigned();
            $table->text('note')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bonus');
    }
}
