<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id')->unique();
            $table->string('parent_id')->nullable()->default(null);
            $table->string('name', 20);
            $table->string('email')->nullable()->default(null)->unique();
            $table->string('picture')->nullable()->default(null);
            $table->tinyInteger('gender')->nullable()->default(null);
            $table->string('google_id')->unique();
            $table->integer('click')->unsigned();
            $table->integer('total_coin')->unsigned();
            // $table->tinyInteger('commission')->nullable()->default(null);
            $table->integer('real_wallet')->unsigned();
            $table->string('agency_code');
            // $table->tinyInteger('agency_percent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
