<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->integer('success')->unsigned()->nullable()->default(0);
            $table->float('rate', 20, 15);
            $table->integer('amount')->unsigned()->nullable()->default(0);
            $table->tinyInteger('get_coin')->nullable()->default(0);
            $table->string('date', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tracking');
    }
}
