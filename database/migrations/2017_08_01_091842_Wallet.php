<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Wallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wallet_key');
            $table->string('wallet_name', 20)->nullable()->default(null);
            $table->integer('wallet_pending')->unsigned();
            $table->tinyInteger('online_status');
            $table->string('online_time')->nullable()->default(null);
            $table->string('online_user')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallets');
    }
}
