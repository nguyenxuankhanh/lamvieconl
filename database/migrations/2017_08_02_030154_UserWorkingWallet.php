<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserWorkingWallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_working_wallet', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('wallet_key');
            $table->tinyInteger('status');
            $table->integer('wallet_pending_start')->unsigned();
            $table->integer('wallet_pending_stop')->unsigned()->nullable()->default(NULL);
            $table->string('time_start', 20);
            $table->string('time_stop', 20)->nullable()->default(NULL);
            $table->text('note')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_working_wallet');
    }
}
