<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Frontend
// Dashboad
Route::get('dashboard', ['as'=>'dashboard','uses'=>'HomeController@dashboard']);

// User
Route::get('home', ['as'=>'home','uses'=>'UserController@index']);
Route::get('login-google', 'UserController@loginGoogle');
Route::get('link-account',['as'=>'link-account','uses'=>'UserController@linkAccount']);
Route::get('work',['as'=>'work','uses'=>'UserController@work']);
Route::get('logout',['as'=>'logout','uses'=>'UserController@logout']);

// Wallet
Route::post('set-wallet/{user_id}',['as'=>'set-wallet','uses'=>'WalletController@setWallet']);

// Checkout
Route::get('history-checkout',['as'=>'history-checkout','uses'=>'CheckoutController@historyCheckout']);

// Rate
Route::post('get-rate',['as'=>'get-rate','uses'=>'RateController@getRate']);

// Backend
