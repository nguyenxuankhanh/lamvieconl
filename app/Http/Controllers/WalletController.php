<?php

namespace App\Http\Controllers;

use App\UserWorkingWallet;
use App\Wallet;
use Session;
use Illuminate\Http\Request;

use App\Http\Requests;
use Symfony\Component\HttpFoundation\Response;

class WalletController extends Controller
{
    public function setWallet($user_id) {
        $result = array();
        $wallet = array();
        $user_id = trim($user_id);
        // Check Wallet which user is used, column online_user.
        $objWallet = new Wallet();
        $objUserWorkingWallet = new UserWorkingWallet();

        if ($objWallet->checkUserIsWorking($user_id)) {
            // User working.
            // Get wallet_key.
            $wallet = $objWallet->getWalletIsWorkingByUser($user_id);
            $result['status'] = 1;
            $result['wallet'] = $wallet['wallet_key'];
        }
        else {
            // User not working.
            // Get wallet is free.
            $wallet = $objWallet->getWalletIsFree($user_id);
            if (!is_null($wallet)) {
                $result['status'] = 1;
                $result['wallet'] = $wallet->wallet_key;
                // Update Pending of Wallet.
                updatePendingWallet($wallet->wallet_key);

                // Insert table user_working_wallet.
                $date = new \DateTime();
                $dataUserIsWorking = array(
                    'user_id' => $user_id,
                    'wallet_key' => $wallet->wallet_key,
                    'status' => STATUS_WORKING,
                    'wallet_pending_start' => $wallet->wallet_pending,
                    'time_start' => $date->format('Y-m-d H:i:s'),
                );
                $objUserWorkingWallet->addUserIsWorking($dataUserIsWorking);
            }
            else {
                $result['status'] = 0;
                $result['message'] = "Các ví đã dùng hết, vui lòng quay lại sau";
            }
        }

        // Set session[working].
        if ($result['status'] == 1) {
            Session::set('working', $result);
        }

        print_r(json_encode($result));
    }
}
