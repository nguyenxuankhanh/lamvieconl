<?php 
namespace App\Http\Controllers;

use App\Checkout;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Rate;
use App\UserCommission;
use App\User;

use Session;
//use App;
use App\Setting;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	// $this->middleware('auth');
	// }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
//		$authUrl = $this->client->createAuthUrl();
//		return view('home', compact('authUrl'));
	}

	public function dashboard() {
        $objSetting = new Setting();
        $setting = $objSetting->getSetting();
        $userData = Session::get('userData');
        $urlRef = url('?ref=' . $userData['agency_code']);

        $data['userData'] = $userData;
        $data['parent_percent'] = $setting['parent_percent'];
        $data['urlRef'] = $urlRef;

        // 1. Top 10 people which have most coin.

        // 2. History checkout.
        $objCheckout = new Checkout();
        $arrCheckout = $objCheckout->getCheckoutByUserId(trim($userData['user_id']));

        // 3. Chart rate.
        // Call by JS.

        // 4. List child_user have commission.
        $objUserCommission = new UserCommission();
        $userCommission = $objUserCommission->getByUserId(trim($userData['user_id']));
        // Handle data to show.
        $total_commission = sizeof($userCommission);
        $total_coin = 0;
        $arrUserChild = array();
        $objUser = new User();
        foreach ($userCommission as $key => $value) {
            $arrUserChild[$key] = $objUser->getUserByUserID($value['child_id']);
            $arrUserChild[$key]['coin'] = $value['coin'];
            $arrUserChild[$key]['created_date'] = date('Y-m-d', strtotime($arrUserChild[$key]['created_at']));
            $total_coin = $total_coin + $value['coin'];
        }
        $data['total_commission'] = $total_commission;
        $data['total_coin'] = $total_coin;
        $data['userChild'] = $arrUserChild;
        $data['checkout'] = $arrCheckout;

        return view('frontend.page.dashboard', compact('data'));
    }

}
