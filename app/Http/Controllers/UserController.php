<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Setting;
use App\Wallet;
use App\UserWorkingWallet;

use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Session;
use App;
use Google_Client;
use Google_Service_Oauth2;

class UserController extends Controller {

    private $client;
    private $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->client = new Google_Client();
        $this->client = new Google_Client();
        $this->client->setClientId(CLIENT_ID);
        $this->client->setClientSecret(CLIENT_SECRET);
        $this->client->setRedirectUri(REDIRECT_URI);
        $this->client->addScope("email");
        $this->client->addScope("profile");
        $this->service = new Google_Service_Oauth2($this->client);
        // $this->middleware('auth');
    }

    public function index(Request $request) {
        if (isset($request->ref)) {
            $data_ref = array(
                'agency_code' => $request->ref,
            );
            Session::set('ref', $data_ref);
        }

        $authUrl = $this->client->createAuthUrl();
        return view('home', compact('authUrl'));
    }

    public function loginGoogle(Request $request) {
        // Login
        if(isset($request->code)) {
            $code = $request->code;
            $this->client->authenticate($code);
            Session::set('gg_access_token', $this->client->getAccessToken());
            $this->client->setAccessToken($this->client->getAccessToken());
            $google_user = $this->service->userinfo->get();

            $data_array = array(
                "google_id" => $google_user->id,
                "token" => TOKEN
            );

            // Check have user in table.
            $objuser = new User;
            if ($objuser->checkGoogleUser($data_array)) {
                // Update user.
                $dataUser = array(
                    'name' => $google_user->name,
                    'email' => $google_user->email,
                    'picture' => $google_user->picture,
                    'gender'  => $google_user->gender,
                    'google_id' => $google_user->id,
                    'agency_code' => 'test update',
                );
                $objuser->updateUser($dataUser);
            }
            else {
                // Add user.
                $postData = array(
                   'user_id' => $google_user->id,
                   'name' => $google_user->name,
                   'email' => $google_user->email,
                );
                $agency_code = md5(serialize($postData));

                // Add agency_code if have ref.
                $parent_id = '';
                if(Session::has('ref')) {
                    // Get user_id by ref.
                    $ref = Session::get('ref');
                    $agency_code = $ref['agency_code'];
                    $user_parent = $objuser->getUserByAgencyCode($agency_code);
                    $parent_id = $user_parent['user_id'];
                }

                $dataUser = array(
                    'user_id' => $google_user->id,
                    'parent_id' => $parent_id,
                    'name' => $google_user->name,
                    'email' => $google_user->email,
                    'picture' => $google_user->picture,
                    'gender'  => $google_user->gender,
                    'google_id' => $google_user->id,
                    'click' => 0,
                    'total_coin' => 0,
                    'commission' => 0,
                    'real_wallet' => 0,
                    'agency_code' => $agency_code,
                    'agency_percent' => 0,
                );

                $objuser->addUser($dataUser);
            }

            // Add Session userData.
            if (Session::has('userData')) {
                return redirect()->route('link-account');
            }
            else {
                // Get exchange_rate;
                $objSetting = new Setting();
                $setting = $objSetting->getSetting();
                $exchange_rate = $setting['exchange_rate'];

                // Get info user.
                $userData = $objuser->getUserByGoogleID($google_user->id);
                $userInfo = array(
                    'user_id'       => $userData->user_id,
                    'parent_id'     => $userData->parent_id,
                    'name'          => $userData->name,
                    'email'         => $userData->email,
                    'picture'       => $userData->picture,
                    'gender'        => $userData->gender,
                    'click'         => $userData->click,
                    'total_coin'    => $userData->total_coin,
                    'real_wallet'   => $userData->real_wallet,
                    'agency_code'   => $userData->agency_code,
                    'commission'    => 100,
                    'exchange_rate' => $exchange_rate,
                );
                Session::set('userData', $userInfo);

                return redirect()->route('work');
            }
        }
    }

    public function linkAccount(){
        $userInfo = Session::get('userData');
        $authUrl = $this->client->createAuthUrl();
        return view('frontend.page.linkAccount', compact('userInfo', 'authUrl'));
    }

    public function work(){
        $objSetting = new Setting();
        $setting = $objSetting->getSetting();
        $userData = Session::get('userData');
        $urlRef = url('?ref=' . $userData['agency_code']);

        $data['userData'] = $userData;
        $data['parent_percent'] = $setting['parent_percent'];
        $data['urlRef'] = $urlRef;

        if (Session::has('working')) {
            $data['working'] = Session::get('working');
        }

        return view('frontend.page.work', compact('data'));
    }

    public function logout(){
        $working = Session::get('working');
        $userData = Session::get('userData');
        $objWallet = new Wallet();
        $objUserWorkingWallet = new UserWorkingWallet();

        $arrPending = getPending($working['wallet']);
        if ($arrPending['status'] == 1) {
            // Update online_status of wallet.
            $dataWallet = array(
                'wallet_key' => $working['wallet'],
                'online_status' => ONLINE_STATUS_FREE,
                'wallet_pending' => $arrPending['pending'],
            );
            $objWallet->updateWallet($dataWallet);
        }
        else {
            // Update online_status of wallet.
            $wallet = $objWallet->getWalletByWalletKey($working['wallet']);
            $dataWallet = array(
                'wallet_key' => $wallet['wallet_key'],
                'online_status' => ONLINE_STATUS_FREE,
                'wallet_pending' => $wallet['wallet_pending'],
            );
            $objWallet->updateWallet($dataWallet);
            // Update pending.
            $arrPending['pending'] = $wallet['wallet_pending'];
        }

        // Update status of user_working_wallet.
        $date = new \DateTime();
        $updateUserData = array(
            'user_id' => $userData['user_id'],
            'wallet_key' => $working['wallet'],
            'status' => STATUS_WORKING,
            'wallet_pending_stop' => $arrPending['pending'],
            'time_stop' => $date->format('Y-m-d H:i:s'),
            'note' => STOP_BY_LOGOUT,
        );
        $objUserWorkingWallet->stopUserWorking($updateUserData);

        // Remove user data from session
        Session::forget('userData');
        Session::forget('working');
        Session::forget('ref');
        Session::forget('gg_access_token');

        return redirect()->route('home');
    }
}
