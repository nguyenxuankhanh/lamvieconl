<?php

namespace App\Http\Controllers;

use App\Rate;
use Illuminate\Http\Request;

use App\Http\Requests;

class RateController extends Controller
{
    public function getRate(Request $request) {
        $data = $request->all(); // This will get all the request data.
        $year = $data['year'];
        $month = $data['month'];
        $day = $data['day'];
        $month = $month < 10 ? '0' . $month : $month;
        $day = $day < 10 ? '0' . $day : $day;

        // Get day Rate.
        $objRate = new Rate();
        $timeDay = $year . '-' . $month . '-' . $day;
        $rateDay = $objRate->getRateDay(trim($timeDay));
        $count = array();
        $rate_chart = array();
        $rate_values = array();
        $checkDateNow = strtotime(date("Y-m-d")) == strtotime($timeDay) ? TRUE : FALSE ;
        $hourNow = date('H', time());

        foreach ($rateDay as $key => $value) {
            $timeStamp = strtotime($value['time']);
            $rateDay[$key]['time'] = $timeStamp;
            $rateDay[$key]['hour'] = date('H', $timeStamp );
            $count[$key] = $timeStamp;
        }
        // Sort array.
        array_multisort($count, SORT_ASC, $rateDay);

        // Fix data if not enough 24 hour.
        $arrHourRate = array();
        for($i = 0; $i < 24; $i++) {
            foreach ($rateDay as $key => $value) {
                if ($i == (int)$value['hour']) {
                    $arrHourRate[$i] = $value;
                    break;
                }
            }
            if (!isset($arrHourRate[$i])) {
                if ($i == 0) {
                    $arrHourRate[$i] = array(
                        'hour' => '00',
                        'rate_display' => 0,
                    );
                }
                else {
                    // Gio tuong lai => rate = 0;
                    if ($checkDateNow && $i >= (int)$hourNow) {
                        $arrHourRate[$i] = array(
                            'hour' => $i,
                            'rate_display' => 0,
                        );
                    }
                    else {
                        $arrHourRate[$i] = array(
                            'hour' => $i,
                            'rate_display' => $arrHourRate[$i-1]['rate_display'],
                        );
                    }
                }
            }
        }

        foreach ($arrHourRate as $key => $value) {
            $rate_chart[$key] = $value['rate_display'];
            $rate_values[] = array($value['hour'] . 'h', $value['rate_display']);
        }

        $result['data']['rate_chart'] = $rate_chart;
        $result['data']['rate_values'] = $rate_values;

        print_r(json_encode($result));
    }
}
