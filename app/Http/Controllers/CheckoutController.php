<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use App\Checkout;

class CheckoutController extends Controller
{
    public function historyCheckout(){
        $userData = Session::get('userData');

        // History checkout.
        $objCheckout = new Checkout();
        $objSetting = new Setting();
        $setting = $objSetting->getSetting();

        $arrCheckout = $objCheckout->getCheckoutByUserId(trim($userData['user_id']));
        foreach ($arrCheckout as $key => $value) {
            $arrCheckout[$key]['status_text'] = $setting['status_' . $value['status']];
        }

        $data['checkout'] = $arrCheckout;
        return view('frontend.page.historyCheckout', compact('data'));
    }
}
