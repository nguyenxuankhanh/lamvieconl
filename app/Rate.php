<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'rate';

    protected $fillable = [
        'rate',
        'user_percent',
        'rate_display',
        'time',
    ];

    public $timestamps = true;

    public function getAll() {
        $rate = Rate::all()->toArray();
        return $rate;
    }

    public function getRateDay($timeDay) {
        $rate = Rate::where('time', 'like' , '%' . $timeDay . '%')
            ->get()
            ->toArray();
        return $rate;
    }
}
