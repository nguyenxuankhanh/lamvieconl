<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    protected $fillable = [
        'name',
        'value'
    ];

    public $timestamps = true;

    public function getSetting() {
        $setting = Setting::all()->toarray();
        $results = array();
        foreach ($setting as $value) {
            $results[$value['name']] = $value['value'];
        }
        return $results;
    }
}
