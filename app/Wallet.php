<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallets';

    protected $fillable = [
        'wallet_key',
        'wallet_name',
        'wallet_pending',
        'online_status',
        'online_time',
        'online_user',
    ];

    public $timestamps = true;

    public function checkUserIsWorking($user_id) {
        $result = FALSE;
        // online_status = 1; wallet is used.
        $wallet = Wallet::where("online_user", '=' , $user_id)
            ->where("online_status", '=', ONLINE_STATUS_USED)
            ->first();
        if (!is_null($wallet)) {
            $result = TRUE;
        }
        return $result;
    }

    public function getWalletIsWorkingByUser($user_id) {
        // online_status = 1; wallet is used.
        $wallet = Wallet::where("online_user", '=' , $user_id)
            ->where("online_status", '=', ONLINE_STATUS_USED)
            ->first()
            ->toArray();
        return $wallet;
    }

    public function getWalletIsFree($user_id) {
        // Update table wallet: online_status, online_time, online_user.
        $wallet = Wallet::where("online_status", '=', ONLINE_STATUS_FREE)
            ->first();

        if(!is_null($wallet)) {
            $date = new \DateTime();
            $wallet->online_status = ONLINE_STATUS_USED;
            $wallet->online_time = $date->getTimestamp();
            $wallet->online_user = $user_id;
            $wallet->save();
        }

        return $wallet;
    }

    public function getWalletByWalletKey($wallet_key) {
        $wallet = Wallet::where("wallet_key", '=', $wallet_key)
            ->first()
            ->toArray();
        return $wallet;
    }

    public function updateWallet($data) {
        $wallet = Wallet::where("wallet_key", '=', $data['wallet_key'])
            ->first();

        if(!is_null($wallet)) {
            $wallet->online_status = $data['online_status'];
            $wallet->wallet_pending = $data['wallet_pending'];
            $wallet->save();
        }
    }
}
