<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWorkingWallet extends Model
{
    protected $table = 'user_working_wallet';

    protected $fillable = [
        'user_id',
        'wallet_key',
        'status',
        'wallet_pending_start',
        'wallet_pending_stop',
        'time_start',
        'time_stop',
        'note',
    ];

    public $timestamps = true;

    public function addUserIsWorking($data){
        $userWorkingWallet = new UserWorkingWallet;
        $userWorkingWallet->user_id         = $data['user_id'];
        $userWorkingWallet->wallet_key      = $data['wallet_key'];
        $userWorkingWallet->status          = $data['status'];
        $userWorkingWallet->wallet_pending_start = $data['wallet_pending_start'];
        $userWorkingWallet->time_start      = $data['time_start'];
        $userWorkingWallet->save();
    }

    public function stopUserWorking($data) {
        $userWorkingWallet = UserWorkingWallet::where("user_id", '=' , $data['user_id'])
            ->where("wallet_key", '=' , $data['wallet_key'])
            ->where("status", '=' , $data['status'])
            ->first();

        if (!is_null($userWorkingWallet)) {
            $userWorkingWallet->status = STATUS_STOP;
            $userWorkingWallet->wallet_pending_stop = $data['wallet_pending_stop'];
            $userWorkingWallet->time_stop = $data['time_stop'];
            $userWorkingWallet->note = $data['note'];
            $userWorkingWallet->save();
        }
    }
}
