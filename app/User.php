<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 
        'parent_id', 
        'name', 
        'email', 
        'picture', 
        'gender', 
        'google_id',
        'click', 
        'total_coin',
        'real_wallet', 
        'agency_code',
    ];

    public $timestamps = true;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    public function checkGoogleUser($data) {
        $result = FALSE;
        $google_id = $data['google_id'];
        $user = User::where("google_id", '=' , $google_id)->first();
        if (!is_null($user)) {
            $result = TRUE;
        }
        return $result;
    }

    public function addUser($data){
        $user = new User;
        $user->user_id      = $data['user_id'];
        $user->parent_id    = $data['parent_id'];
        $user->name         = $data['name'];
        $user->email        = $data['email'];
        $user->picture      = $data['picture'];
        $user->gender       = $data['gender'];
        $user->google_id    = $data['google_id'];
        $user->click        = $data['click'];
        $user->total_coin   = $data['total_coin'];
        $user->real_wallet  = $data['real_wallet'];
        $user->agency_code  = $data['agency_code'];
        $user->save();
    }

    public function updateUser($data){
        $user = User::where("google_id", '=' , $data['google_id'])->first();
        $user->name         = $data['name'];
        $user->email        = $data['email'];
        $user->picture      = $data['picture'];
        $user->gender       = $data['gender'];
        $user->save();
    }

    public function getUserByGoogleID($google_id) {
        $user = User::where("google_id", '=' , $google_id)->first();
        return $user;
    }

    public function getUserByAgencyCode($agency_code) {
        $user = User::where("agency_code", '=' , $agency_code)->first()->toArray();
        return $user;
    }

    public function getUserByUserID($user_id) {
        $user = User::where("user_id", '=' , $user_id)->first()->toArray();
        return $user;
    }
}
