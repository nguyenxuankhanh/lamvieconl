<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCommission extends Model
{
    protected $table = 'user_commission';

    protected $fillable = [
        'user_id',
        'child_id',
        'coin',
    ];

    public $timestamps = true;

    public function getByUserId($user_id) {
        $user_commission = UserCommission::where('user_id', '=', $user_id)
            ->get()
            ->toArray();
        return $user_commission;
    }
}
