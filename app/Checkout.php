<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $table = 'checkout';

    protected $fillable = [
        'user_id',
        'amount',
        'status',
        'bitcoin_key',
        'payment_info',
    ];

    public $timestamps = true;

    public function getCheckoutByUserId($user_id) {
        $checkout = Checkout::where("user_id", '=', $user_id)
            ->get()
            ->toArray();
        return $checkout;
    }
}
