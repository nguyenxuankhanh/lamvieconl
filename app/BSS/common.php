<?php
use GuzzleHttp\Client;
use \App\Wallet;

function stripUnicode($str){
    if(!$str) return false;
    $unicode = array(
        'a' =>'á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ',
        'A' =>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'd' =>'đ',
        'D' =>'Đ',
        'e' =>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
        'E' =>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'i' =>'í|ì|ỉ|ĩ|ị',
        'I' =>'Í|Ì|Ỉ|Ĩ|Ị',
        'o' =>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
        'O' =>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'u' =>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
        'U' =>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'y' =>'ý|ỳ|ỷ|ỹ|ỵ',
        'Y' =>'Ý|Ỳ|Ỷ|Ỹ|Ỵ'
    );
    foreach($unicode as $khongdau=>$codau) {
        $arr=explode("|",$codau);
        $str = str_replace($arr,$khongdau,$str);
    }
    return $str;
}

function changeTitle($str){
    $str=trim($str);
    if ($str=="") return "";
    $str =str_replace('"','',$str);
    $str =str_replace("'",'',$str);
    $str = stripUnicode($str);
    $str = mb_convert_case($str,MB_CASE_LOWER,'utf-8');
    // MB_CASE_UPPER / MB_CASE_TITLE / MB_CASE_LOWER
    $str = str_replace(' ','-',$str);
    return $str;
}

function getPending($wallet_key) {
    $result = array();
    $uri = 'https://faucet.raiblockscommunity.net/paylist.php?acc='. $wallet_key . '&json=1';
    $client = new Client();

    $res = $client->request('GET', $uri);
    if ($res->getStatusCode() == 200) {
        $result['status'] = 1;
        $content = json_decode($res->getBody()->getContents());
        $pending = 0;

        foreach ($content->pending as $key=>$item) {
            if(isset($item->pending) &&
                ($item->account == $wallet_key)){
                $pending = $item->{'ask-till-now'};
            }
        }
        $result['status'] = 1;
        $result['pending'] = $pending;
    }
    else {
        $result['status'] = 0;
        $result['pending'] = 0;
    }

    return $result;
}

function updatePendingWallet($wallet_key) {
    $arrPending = getPending($wallet_key);
    $objWallet = new Wallet();
    if ($arrPending['status'] == 1) {
        // Update online_status of wallet.
        $wallet = $objWallet->getWalletByWalletKey($wallet_key);
        $dataWallet = array(
            'wallet_key' => $wallet['wallet_key'],
            'online_status' => $wallet['online_status'],
            'wallet_pending' => $arrPending['pending'],
        );
        $objWallet->updateWallet($dataWallet);
    }
}
