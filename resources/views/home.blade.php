<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home</title>

    <link rel="icon" href="images/favicon.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon"/>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="{{ url('public/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{url('public/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->

    <script src="{{ url('public/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
    <script src="{{ url('public/js/matchheight.js') }}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="{{ url('public/css/mycustom.css') }}" rel="stylesheet">
    <script>
    jQuery(document).ready(function(){
      jQuery("#navbar a,#navbar-footer a").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
          // Store hash
          var hash = this.hash;
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          jQuery('html, body').animate({
            scrollTop: jQuery(hash).offset().top
          }, 800, function(){
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });

      jQuery(".requirement").matchHeight();

      jQuery('.home-slider').slick({
        arrows: false
      });

      jQuery('.testimonial-slider').slick({
        arrows: false,
        slidesToShow: 2,
        centerMode: true,
        centerPadding: '200px',
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
          {
            breakpoint: 900,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerPadding: '100px'
            }
          },
          {
            breakpoint: 500,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              centerPadding: '0px'
            }
          }
        ]
      });

      jQuery(".earn-money-button").click(function(){
        <?php if(!isset($_SESSION['userData']['id'])): ?>
        jQuery('#earn-money-popup').modal('show');
        <?php else: ?>
        window.location.href = "work.php";
        <?php endif; ?>
      });

      jQuery(".toggleFaq").click(function(){
        jQuery(this).closest(".item").find(".answer").collapse('toggle');
      });
    });
    </script>

  </head>
  <body class="homepage">
    <section class="header" id="header">
      <div class="container">
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header col-md-3">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">
                <img src="{{ url('public/images/logo.png') }}" alt="">
              </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#header">TRANG CHỦ</a></li>
                <li><a href="#benefit">QUYỀN LỢI</a></li>
                <li><a href="#top-customer">TOP 10</a></li>
                <li><a href="#instruction">HƯỚNG DẪN</a></li>
                <li><a href="#testimonial">BÌNH LUẬN</a></li>
                <li><a href="#faqs">FAQs</a></li>
              </ul>
              <button class="pull-right earn-money-button">Kiếm tiền ngay</button>
            </div><!--/.nav-collapse -->
          </div><!--/.container-fluid -->
        </nav>
      </div>
    </section>
    <section class="slider">
      <div class="container-fluid">
        <div class="home-slider">
          <!-- <div class="banner-item">
            <div class="banner-info container">
              <div class="row">
                <div class="col-md-6">
                  <h3>Bạn muốn trở thành nguồn thu nhập chính của gia đình ngay từ khi ngồi trên ghế nhà trường ?</h3>
                  <ul>
                    <li>Việc Làm Online</li>
                    <li>Tự Chủ Thời Gian</li>
                    <li>Kiếm Tiền Nhanh Chóng</li>
                  </ul>
                  <button class="earn-money-button">Kiếm tiền ngay</button>
                </div>
              </div>
            </div>
          </div> -->
          <div class="banner-item1">
            <div class="banner-info">
              <div class="row">
                <img src="{{ url('public/images/banner.jpg') }}" class="img-responsive" width="100%" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="benefit" id="benefit">
      <div class="container text-center">
        <h2 class="section-title">QUYỀN LỢI CỦA BẠN</h2>
        <div class="row">
          <div class="col-md-4">
            <img src="{{ url('public/images/icon1.png') }}" />
            <h3>Thoải mái về thời gian</h3>
            <p>Đa số các công việc hiện nay đều bị ép buộc về thời gian và số lượng nhưng công việc của chúng tôi mang lại thì hoàn toàn ngược lại</p>
          </div>
          <div class="col-md-4">
            <img src="{{ url('public/images/icon2.png') }}" />
            <h3>Công việc dễ dàng</h3>
            <p>Công việc này bạn chỉ cần chọn hình ảnh theo yêu cầu hệ thống nên không đòi hỏi bất kì chuyên môn hay bằng cấp. Ai cũng có thể thực hiện công việc này</p>
          </div>
          <div class="col-md-4">
            <img src="{{ url('public/images/icon3.png') }}" />
            <h3>Không bị ép về số lượng</h3>
            <p>Công việc này đòi hỏi sự chăm chỉ của các bạn. Ai làm nhiều thì được hưởng nhiều , ai làm ít thì hưởng ít.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <img src="{{ url('public/images/icon4.png') }}" />
            <h3>Môi trường làm việc linh động</h3>
            <p>Chỉ cần 1 chiếc smartphone hoặc máy tính kết nối internet bạn có thể làm việc ở bất cứ đâu</p>
          </div>
          <div class="col-md-4">
            <img src="{{ url('public/images/icon5.png') }}" />
            <h3>Thi đua tăng thu nhập</h3>
            <p>Hằng tháng chúng tôi sẽ có các sự kiện đua top hấp dẫn giúp bạn tăng thêm thu nhập. Điều này rất phù hợp với những bạn  làm việc chăm chỉ.</p>
          </div>
          <div class="col-md-4">
            <img src="{{ url('public/images/icon6.png') }}" />
            <h3>Thanh toán chuyên nghiệp</h3>
            <p> Hệ thống thanh toán của chúng tôi hoàn toàn tự động, chỉ cần bạn làm đủ là có thể đổi thẻ cào hoặc rút về tài khoản</p>
          </div>
        </div>
      </div>
    </section>
    <section class="top-customer" id="top-customer">
      <div class="container-fluid">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <h2>Thi đua tăng thu nhập</h2>
              <p>Khi bạn là người giỏi nhất, chăm chỉ nhất. Bạn dành công sức và thời gian của mình vào công việc bạn đang làm để tăng thu nhập phụ giúp gia đình. Rõ ràng bạn chính là người xứng đáng nhất để chúng tôi trao thưởng!</p>
              <div class="row bonus">
                <div class="col-md-4 border-right">
                  <h3>TOP 1</h3>
                  <p>Người giỏi nhất</p>
                </div>
                <div class="col-md-8">
                  <h3>+ 200.000 VNĐ</h3>
                  <p>Được cộng thêm vào tài khoản của tháng</p>
                </div>
              </div>
              <button class="earn-money-button blue-button">Kiếm tiền ngay</button>
            </div>
            <div class="col-md-6">
              <div class="top-customer-table">
                <table class="table">
                  <tr>
                    <th colspan="2" class="text-center">TOP 10 THÀNH VIÊN GIỎI NHẤT</th>
                  </tr>
                 
                </table>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>
    <section class="instruction" id="instruction">
      <div class="container text-center">
        <div class="row">
          <div class="requirement col-xs-6 col-md-3 border-right">
            <h4>1</h4>
            <p>Chiếc laptop hoặc smartphone</p>
          </div>
          <div class="requirement col-xs-6 col-md-3 border-right">
            <h4>1</h4>
            <p>Góc làm việc yên tĩnh</p>
          </div>
          <div class="requirement col-xs-6 col-md-3 border-right">
            <h4>8</h4>
            <p>Giờ làm việc mỗi ngày</p>
          </div>
          <div class="requirement col-xs-6 col-md-3">
            <h4>200.000</h4>
            <p>Là số tiền tối thiểu bạn kiếm được</p>
          </div>
        </div>
        <button class="earn-money-button">Kiếm tiền ngay</button>
        <h2 class="section-title">HƯỚNG DẪN KIẾM TIỀN</h2>
        <div class="row">
          <div class="col-md-7 video">
            <iframe width="100%" height="360" src="https://www.youtube.com/embed/CevxZvSJLk8" frameborder="0" allowfullscreen></iframe>
          </div>
          <div class="col-md-4 best-customer">
            <div class="">
              <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
              <p class="name">Lê Tuấn Thanh - <span>Thành viên xuất sắc khóa I</span></p>
              <p class="comment">«Mình dành những kinh nghiệm quý báu của mình để soạn bộ video hướng dẫn này, hy vọng sẽ giúp các bạn làm việc hiệu quả hơn với LamviecOnl»</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="testimonial text-center" id="testimonial">
      <div class="container-fluid">
        <div class="testimonial-slider">
          <div class="banner-item borderbox">    
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
          <div class="banner-item">
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
          <div class="banner-item borderbox">
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
          <div class="banner-item borderbox">
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
          <div class="banner-item borderbox">
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
          <div class="banner-item borderbox">
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
          <div class="banner-item borderbox">
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
          <div class="banner-item borderbox">
            <img src="{{ url('public/images/img.jpg') }}" alt="..." class="img-circle profile_img" width="70px">
            <p class="name">Vũ Hoàng Sơn</p>
            <p class="address">Năm II Đại Học Bách Khoa</p>
            <p>Công việc cũng khá đơn giản với mình, chỉ cần chăm chỉ và thu xếp thời gian hợp lí là được.</p>
          </div>
        </div>
      </div>
    </section>
    <section class="faqs" id="faqs">
      <div class="container text-center">
        <h2 class="section-title">FAQs</h2>
        <div class="row">
          <div class="col-md-6">
            <div class="item">
              <div class="borderbox">
                <p class="question">Tại sao mình làm đúng liên tục nhưng vẫn chưa thấy Xu tăng?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Xu sẽ được cập nhật vào tài khoản 1 tiếng / 1 lần</p>
              </div>
            </div>
            <div class="item">
              <div class="borderbox">
                <p class="question">Nên làm việc vào thời điểm nào để thu được nhiều Xu nhất?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Chúng tôi khuyên bạn nên làm vào 8h sáng đến 6h tối (thời điểm này không có nhiều Tài khoản làm việc, nên tỷ giá Xu sẽ cao hơn)</p>
              </div>
            </div>
            <div class="item">
              <div class="borderbox">
                <p class="question">Trung bình một ngày mình có thêm kiếm bao nhiêu tiền?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Số tiền bạn kiếm được phụ thuộc vào số lượng Hình Ảnh bạn làm được nhiều hay ít. Trung bình nếu một ngày làm 8 tiếng, bạn có thể kiếm được 150.000đ - 200.000đ (khoảng 10.000 hình)</p>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="item">
              <div class="borderbox">
                <p class="question">Tại sao mình click vào "Làm Việc Ngay" nhưng hệ thống không hoạt động?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Thỉnh thoảng lỗi này sẽ xảy ra, bạn có thể F5/reload lại trang (Điều này không ảnh hưởng đến số Xu hiện tại của bạn)</p>
              </div>
            </div>
            <div class="item">
              <div class="borderbox">
                <p class="question">Hình ảnh recaptcha xuất hiện rất chậm làm ảnh hưởng đến công việc của mình làm cách nào để khắc phục?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Recaptcha phụ thuộc vào Google, để khắc phục tình trạng này, bạn nên đăng nhập gmail trong trình duyệt và kiểm tra lại đường truyền mạng.</p>
              </div>
            </div>
            <!-- <div class="item">
              <div class="borderbox">
                <p class="question">Sao mình làm mà coin cộng vào tài khoản bị thiếu?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Trước khi tắt cửa sổ làm việc bạn nên để 5-10 phút để hệ thống xác nhận hoàn toàn rồi mới tắt</p>
              </div>
            </div>
            <div class="item">
              <div class="borderbox">
                <p class="question">Hình ảnh recaptcha xuất hiện rất chậm làm ảnh hưởng đến công việc của mình làm cách nào để khắc phục?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Recaptcha phụ thuộc vào google để tránh bị lag bạn nên đăng nhập gmail trong trình duyệt và kiểm tra lại đường truyền mạng</p>
              </div>
            </div>
            <div class="item">
              <div class="borderbox">
                <p class="question">Công việc này có lâu dài không?</p>
                <button class="toggleFaq pull-right">XEM CÂU TRẢ LỜI</button>
                <div class="clearfix"></div>
                <p class="answer collapse">Công việc này phụ thuộc vào đối tác nhưng chúng tôi bảo đảm sẽ tồn tại ít nhất 1 năm</p>
              </div>
            </div> -->
          </div>
        </div>
        <button class="earn-money-button">Kiếm tiền ngay</button>
      </div>
    </section>
    <section class="footer">
      <div class="container-fluid">
        <div class="container">      
          <img src="{{ url('public/images/logo-footer.png') }}" alt="">
          <div id="navbar-footer" class="navbar-collapse collapse pull-right">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#header">TRANG CHỦ</a></li>
              <li><a href="#benefit">QUYỀN LỢI</a></li>
              <li><a href="#top-customer">TOP 10</a></li>
              <li><a href="#instruction">HƯỚNG DẪN</a></li>
              <li><a href="#testimonial">BÌNH LUẬN</a></li>
              <li><a href="#faqs">FAQs</a></li>
            </ul>
          </div>
          <div class="clearfix"></div>
          <ul class="social">
            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
          </ul>
          <p class="copyright pull-right">Lamvieconl 2017 © All rights reserved.</p>
        </div>
      </div>
    </section>

    <!-- Modal -->

    <!-- Get login url -->
      <div class="modal fade text-center" id="earn-money-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Đăng nhập và làm việc</h4>
            </div>
            <div class="modal-body">
              <a href="<?php echo $authUrl; ?>" class="loginBtn loginBtn--google">Login with Google</a>
              <p>*Chúng tôi sẽ đảm bảo thông tin cá nhân của bạn được bảo mật</p>
            </div>
          </div>
        </div>
      </div>
  
  </body>
</html>

