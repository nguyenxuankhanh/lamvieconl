<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('pageTitle')</title>

    <link rel="icon" href="{{ url('public/images/favicon.png') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ url('public/images/favicon.png') }}" type="image/x-icon"/>

    <!-- Bootstrap -->
    <link href="{{ url('public/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="{{ url('public/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ url('public/css/custom.min.css') }}" rel="stylesheet">
    <link href="{{ url('public/css/mycustom.css') }}" rel="stylesheet">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script> -->
    <?php // include "script.php"; ?>
    <script src="{{ url('public/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ url('public/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('public/js/matchheight.js') }}"></script>
    <script type="text/javascript" src="{{ url('public/js/copy2clipboard.js') }}"></script>
    <script type="text/javascript" src="{{ url('public/js/jscharts.js') }}"></script>
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <!-- Day la noi chua noi dung -->
            @include('frontend.menu')
            @yield('content')
            <!-- End Day la noi chua noi dung -->
        </div>
    </div>
    @include('frontend.footer')

<script>
    jQuery(".link-account .info").matchHeight();
    jQuery(document).ready(function(){
        <?php if(isset($_SESSION['showMessageLinkAccount'])):
          unset($_SESSION['showMessageLinkAccount']);
          ?>
        $('#popup').modal('show');
        <?php endif; ?>
        jQuery('.right_col').css('min-height', $(window).height());

        var bodyHeight = jQuery('body').outerHeight(),
            footerHeight = jQuery('body').hasClass('footer_fixed') ? 0 : jQuery('footer').height(),
            leftColHeight = jQuery('.left_col').eq(1).height() + jQuery('.sidebar-footer').height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        //contentHeight -= jQuery('.nav_menu').height() + footerHeight;

        var divHeight = contentHeight - 80;

        jQuery('.right_col').css('min-height', contentHeight);
        jQuery('.right_col > .x_panel').css('min-height', divHeight);
    });
</script>

<?php //include "footer.php"; ?>
</body>
</html>
