<?php $userData = Session::get('userData');?>
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title">
            <a href="/" class="site_title">
                <img src=" {{ url('public/images/logo.png') }}" alt="">
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="<?php echo $userData['picture']; ?>" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <h2><?php echo $userData['name']; ?></h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <div class="clearfix"></div>

        <div id="wallet">
            <p class="title">1. Đã làm được:</p>
            <!-- <p class="value pending_captcha"><span id="totalCaptchas">0</span> Hình Ảnh đang chờ</p> -->
            <input type="hidden" value="<?php echo $userData['click']; ?>" id="totalCaptcha" />
            <p class="value"><span id="totalCaptchas"><?php  echo $userData['click']; ?></span> Hình Ảnh</p>

            <p class="value"><?php echo round($userData['total_coin'],4); ?> Xu</p>
            <p class="title">2. Hoa hồng:</p>
            <p class="value"><?php echo round($userData['commission'],4) ?> Xu</p>
            <p class="title">3. Xu còn lại:</p>
            <p class="value"><?php echo round($userData['real_wallet'],4) ?> Xu</p>
            <p class="title">4. Tỉ giá:</p>
            <p class="value"><?php  echo $userData['exchange_rate']; ?> VND/Xu</p>
            <p class="title">5. Quy đổi:</p>
            <p class="value"><?php echo round($userData['real_wallet'] * $userData['exchange_rate'], 2) ?> VND</p>
            <p style="text-align:center">
                <input id='checkoutButton' type="button" value="Thanh Toán" class="btn btn-primary" data-toggle="modal" data-target="#checkout-popup">
                <button type="button" name="button" id="updateClaim" class="btn btn-primary"><i class="fa "></i> Cập nhật Claim</button>
            </p>
        </div>
        <div class="clearfix"></div>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li class="highlight"><a href="{!! URL::route('work') !!}"><i class="fa fa-list-alt"></i> Làm Việc Ngay </a></li>
                    <li><a href="{!! URL::route('dashboard') !!}"><i class="fa fa-home"></i> Quản Lý</a></li>
                    <li><a href="{!! URL::route('history-checkout') !!}"><i class="fa fa-history"></i> Lịch Sử Thanh Toán </a></li>
                    <!-- <li><a href="#"><i class="fa fa-question-circle-o"></i> Hướng Dẫn </a></li> -->
                    <!-- <li><a href="game.php"><i class="fa fa-money"></i> Quay số may mắn </a></li> -->
                    <!-- <li><a href="report.php"><i class="fa fa-bar-chart"></i> Bảng Thống Kê </a></li>-->
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle visible-xs visible-sm hidden-md">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="<?php echo $userData['picture']; ?>" alt=""><?php echo $userData['name']; ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{!! URL::route('link-account') !!}"><i class="fa fa-link pull-right"></i> Liên kết tài khoản</a></li>
                        <li><a href="{!! URL::route('logout') !!}"><i class="fa fa-sign-out pull-right"></i> Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->