@extends('frontend.master')
@section('pageTitle', 'Liên kết tài khoản')
@section('content')
<!-- page content -->
<div class="right_col" role="main">
    <div class="x_panel">
        <h2 class="page-title">Liên kết tài khoản</h2>
        <div class="row link-account text-center">
            <div class="col-md-12">
                <h3 class="text-center">Google</h3>
                <?php if ($userInfo['user_id']): ?>
                <div class="info">
                    <div class="text-center">
                        <img src="<?php echo $userInfo['picture'] ?>" alt="..." class="img-circle profile_link_img">
                    </div>
                    <p><strong>Tên:</strong> <?php echo $userInfo['name'] ?></p>
                    <p><strong>Email:</strong> <?php echo $userInfo['email'] ?></p>
                </div>
                <?php else: ?>
                <div class="info not-available">
                    <a href="<?php echo $authUrl ?>" class="loginBtn loginBtn--google">Login with Google</a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="popup" tabindex="-1" role="dialog" aria-labelledby="popupLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="popupLabel">Thông báo</h4>
            </div>
            <div class="modal-body">
                <h2>Tài khoản liên kết này đã được sử dụng. Xin vui lòng sử dụng tài khoản khác.</h2>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>
@endsection()