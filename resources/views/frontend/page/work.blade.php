@extends('frontend.master')
@section('pageTitle', 'Làm việc')
@section('content')
<div class="right_col" role="main">
    <div class="x_panel text-center workplace">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <div class="header-logo">
            <img style="width:100%" src="{{ url('public/images/mypapa.jpg') }}" alt="">
            <p>Hãy Down Và Chơi Game Để Ủng Hộ LamViecOnl</p>
            <a href="https://goo.gl/8yHjoM" target="_blank">Dành cho Android</a>
            <br/>
            <a href="https://goo.gl/0JMZkG" target="_blank">Dành cho iOS</a>
        </div>
        <div style="height:40px"></div>
        <p style="color : red;font-size : 30px">
            Chú ý, hãy kiểm tra kỹ Ví trước khi làm. Chúng tôi không chịu trách nhiệm nếu bạn copy sai ví.
        </p>
        <p style="color : red;font-size : 20px">
            1. Click vào kiếm tiền ngay. Một cửa sổ mới sẽ được mở ra.
        </p>

        <p style="color : red;font-size : 20px">
            2. Copy ví được tạo ra và dán vào ô dữ liệu, bấm claim và làm việc như bình thường.
        </p>

        <p style="color : red;font-size : 20px">
            3. Ảnh sau 1 tiếng sẽ tự cộng. Các bạn vui lòng F5 lại. Nếu không cộng cần kiểm tra lại ví
        </p>

        <p style="color : red;font-size : 20px">
            4. Khi không làm việc quá 1 tiếng, hệ thống sẽ tự đăng xuất. Lúc này, hãy đăng nhập lại và nhận link mới.
        </p>

        <p style="font-size : 20px">
            Tham gia Group Facebook để được hỗ trợ và nhận thêm các thông tin khuyến mãi :
            <a target="_blank" href="https://web.facebook.com/groups/222757348218229">
                https://web.facebook.com/groups/222757348218229/
            </a>
        </p>
        <p style="font-size : 20px">
            Tổng hợp hướng dẫn và các cách Fix lỗi :
            <a target="_blank" href="https://www.facebook.com/groups/222757348218229/permalink/237854650041832/">
                https://www.facebook.com/groups/222757348218229/permalink/237854650041832/
            </a>
        </p>
        <div style="height:40px"></div>

        <?php
//        $data_array = array(
//            "user_id" => $_SESSION['userData']['id'],
//            "token"   => $token
//        );
//        $data = json_encode($data_array);
//        $response = requestApi($apiDomain."OWUserIsLogin", $data);
//        $response = json_decode($response);
//        //print_r($response);
//        if(!$response->data){
//            //login
//            $data_array = array(
//                "user_id" => $_SESSION['userData']['id'],
//                "token"   => $token
//            );
//            $data = json_encode($data_array);
//            $response = requestApi($apiDomain."OWLoginUser", $data);
//            $response = json_decode($response);
//            //print_r($response);
//
//            $data = $response->data;
//            if($response->status){
//                $_SESSION['userData']['wallet'] = $response->data;
//            } else {
//                //error
//            }
//        }
        ?>
        <?php if(isset($data['working']) && $data['working']['wallet']): ?>
          <p style="font-weight:bold">
                Chú ý : Ví của bạn ở phiên làm việc hiện tại là : </br>
                <textarea id="txtWallet" class="js-copytextarea" style="width: 100%;text-align: center;">
                    <?php echo $data['working']['wallet'] ?>
                </textarea> </br>
                Hãy copy ví và dán vào ô dữ liệu của web, kiểm tra ô dữ liệu trùng với ví hiện tại
            </p>
            <p>
            <button class="js-textareacopybtn">Copy ví hiện tại</button></p>
            <a href="https://faucet.raiblockscommunity.net/form.php" id='claimButton' class="btn btn-primary" target="_blank" />Làm việc ngay</a>
        <?php else: ?>
            <a id='claimButton' class="btn btn-primary" target="_blank" />Làm việc ngay</a>
            <a href="#" id='workButton' class="btn btn-primary btn-request-checkout" target="_blank" style="display:none" />Làm việc ngay</a>
        <?php endif; ?>

        <p>
            <a href="<?php echo $data['urlRef'] ?>">
                <?php echo $data['urlRef'] ?>
            </a>
        </p>
        <p>Chia sẻ link này để nhận được <?php echo $data['parent_percent']*100 ?>% xu từ người giới thiệu làm</p>
        <div style="height:60px"></div>

    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#claimButton').click(function(){
            $.ajax({
                url: 'set-wallet/ {{ $data['userData']['user_id'] }} ',
                method: 'post',
                data: {_token: $('input[name="_token"]').val()},
                success: function(data){
                    var json = JSON.parse(data);
                    if(json.status == 1){
                        $('#claimButton').hide();

                        $('#workButton').attr("href","https://faucet.raiblockscommunity.net/form.php");
                        $('#workButton').show();
                        $("<p style='font-weight:bold'>" +
                            "Chú ý : Ví của bạn ở phiên làm việc hiện tại là : </br></br>" +
                            "<textarea id='txtWallet' class='js-copytextarea' style='width: 100%;text-align: center;'>"
                            + json.wallet +
                            "</textarea></br>Hãy copy ví và dán vào ô dữ liệu của web, kiểm tra ô dữ liệu trùng với ví hiện tại</p>" +
                            "<p><button class='js-textareacopybtn'>Copy ví hiện tại</button></p>").insertBefore($("#workButton"));
                        $('#workButton').trigger("click");
                    } else {
                        alert(json.message);
                    }
                },
                error: function(){
                    console.log('error');
                },
            });
        });

        // Copy when click.
        $('.js-textareacopybtn').click(function () {
            select_all_and_copy(document.getElementById('txtWallet'));
        });

    });
</script>
@endsection()