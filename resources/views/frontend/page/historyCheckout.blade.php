@extends('frontend.master')
@section('pageTitle', 'Lịch sử thanh toán')
@section('content')
<div class="right_col" role="main">
    <div class="x_panel">
        <h2 class="page-title">Lịch sử thanh toán</h2>
        <table id="history-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>STT</th>
                <th>Thời gian</th>
                <th>Tổng tiền</th>
                <th>Trạng thái</th>
                <th>Chi tiết giao dịch</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($data['checkout'] as $key => $item)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $item['created_at'] }}</td>
                    <td>{{ $item['amount'] }}</td>
                    <td>{{ $item['status_text'] }}</td>
                    <td>{{ $item['payment_info'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    // Table.
    jQuery('.table').DataTable({
        "searching":   false,
        "lengthChange": false,
        "info":     false
    });
</script>
@endsection()