@extends('frontend.master')
@section('pageTitle', 'Quản lý')
@section('content')
<div class="right_col" role="main">
    <div class="">

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">

                        <div class="col-md-4 col-sm-12 col-xs-12">
                            <div>
                                <div class="x_title">
                                    <h2>Top 10 người giỏi nhất</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <?php //
//                                $data_array = array(
//                                    "token" => $token
//                                );
//                                $data = json_encode($data_array);
//                                $response = requestApi($apiDomain."getTop10Users", $data, true);
//                                $response = json_decode($response);
                                ?>
                                <ul class="list-unstyled top_profiles scroll-view">
                                    <?php // foreach ($response->data as $key => $item): ?>
                                    <li class="media event">
                                        <a class="pull-left border-aero profile_thumb">
                                            <img src="<?php // echo $item->avatar ?>" alt="<?php // echo $item->name ?>" class="img-circle profile_img">
                                        </a>
                                        <div class="media-body">
                                            <a class="title" href="#"><?php // echo $item->name ?></a>
                                            <p><?php // echo $item->click_success ?></p>
                                        </div>
                                    </li>
                                    <?php // endforeach; ?>
                                </ul>
                            </div>
                        </div>

                    <!-- <div class="col-md-8 col-sm-12 col-xs-12">
                  <div class="x_title">
                    <h2>Tổng số hình ảnh làm</h2>
                    <div class="clearfix"></div>
                  </div>
                  <?php //
//                    $data_array = array(
//                        "time"    => date("Y-m"),
//                        "user_id" => $_SESSION['userData']['id'],
//                        "token"   => $token
//                    );
//                    $data = json_encode($data_array);
//                    //$response = requestApi($apiDomain."getTrackingByMonth", $data, true);
//                    $response = requestApi($apiDomain."getTrackingByMonth", $data);
//                    $response = json_decode($response);
//
//                    $captcha_chart = array();
//                    $captcha_values = array();
//                    foreach ($response->data->graph as $key => $value) {
//                        $captcha_chart[$key] = $value;
//                        $captcha_values[] = "['".$key."',".$value."]";
//                    }
                    ?>
                            <div class="">
                              <div class="filter captcha_filter">
                                <select class="year form-control">
                                  <option value="2017" <?php // if(date("Y")=="2017") echo 'selected="selected"'; ?>>2017</option>
                        <option value="2016" <?php // if(date("Y")=="2016") echo 'selected="selected"'; ?>>2016</option>
                        <option value="2015" <?php // if(date("Y")=="2015") echo 'selected="selected"'; ?>>2015</option>
                        <option value="2014" <?php // if(date("Y")=="2014") echo 'selected="selected"'; ?>>2014</option>
                      </select>
                      <select class="month form-control">
                        <option value="01" <?php // if(date("n")=="01") echo 'selected="selected"'; ?>>Tháng 01</option>
                        <option value="02" <?php // if(date("n")=="02") echo 'selected="selected"'; ?>>Tháng 02</option>
                        <option value="03" <?php // if(date("n")=="03") echo 'selected="selected"'; ?>>Tháng 03</option>
                        <option value="04" <?php // if(date("n")=="04") echo 'selected="selected"'; ?>>Tháng 04</option>
                        <option value="05" <?php // if(date("n")=="05") echo 'selected="selected"'; ?>>Tháng 05</option>
                        <option value="06" <?php // if(date("n")=="06") echo 'selected="selected"'; ?>>Tháng 06</option>
                        <option value="07" <?php // if(date("n")=="07") echo 'selected="selected"'; ?>>Tháng 07</option>
                        <option value="08" <?php // if(date("n")=="08") echo 'selected="selected"'; ?>>Tháng 08</option>
                        <option value="09" <?php // if(date("n")=="09") echo 'selected="selected"'; ?>>Tháng 09</option>
                        <option value="10" <?php // if(date("n")=="10") echo 'selected="selected"'; ?>>Tháng 10</option>
                        <option value="11" <?php // if(date("n")=="11") echo 'selected="selected"'; ?>>Tháng 11</option>
                        <option value="12" <?php // if(date("n")=="12") echo 'selected="selected"'; ?>>Tháng 12</option>
                      </select>
                      <p class="pull-right">Tổng cộng: <span><?php // echo $response->data->total ?></span></p>
                    </div>
                    <div class="clearfix"></div>
                    <div id="captcha_graph" style="width: 100%;overflow: scroll;position: relative;">Loading graph...</div>
                  </div>

                </div>

              </div> -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_content">

                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div>
                                    <div class="x_title">
                                        <h2>Lịch sử thanh toán</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <table class="table checkout-history table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Thời gian</th>
                                            <th>Tổng tiền</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($data['checkout'] as $key => $item): ?>
                                        <tr>
                                            <td><?php echo $item['created_at']; ?></td>
                                            <td><?php echo $item['amount']; ?></td>
                                            <td>
                                            <?php switch ($item['status']) {
                                                case 0:
                                                    echo '<i class="fa fa-times-circle" aria-hidden="true"></i>';
                                                    break;
                                                case 1:
                                                    echo '<i class="fa fa-spinner" aria-hidden="true"></i></td>';
                                                    break;
                                                case 2:
                                                    echo '<i class="fa fa-check-circle" aria-hidden="true"></i>';
                                                    break;
                                                default:
                                                    # code...
                                                    break;
                                            } ?>
                                        </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="x_title">
                                    <h2>Biểu đồ giá coin thay đổi</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="">
                                    <div class="filter rate_filter">
                                        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                        <select class="year form-control" name="year" id="year" size="1">
                                            <option value="2017" <?php if(date("Y")=="2017") echo 'selected="selected"'; ?>>2017</option>
                                            <option value="2016" <?php if(date("Y")=="2016") echo 'selected="selected"'; ?>>2016</option>
                                            <option value="2015" <?php if(date("Y")=="2015") echo 'selected="selected"'; ?>>2015</option>
                                            <option value="2014" <?php if(date("Y")=="2014") echo 'selected="selected"'; ?>>2014</option>
                                        </select>
                                        <select class="month form-control" name="month" id="month" size="1">
                                            @for ($i = 1; $i <= 12; $i++)
                                                @if(date("n") == $i)
                                                    <option value="{{$i}}" selected="selected">Tháng {{$i}} </option>
                                                @else
                                                    <option value="{{$i}}">Tháng {{$i}} </option>
                                                @endif
                                            @endfor
                                        </select>
                                        <select class="day form-control" name="day" id="day" size="1">
                                            @for ($i = 1; $i <= 31; $i++)
                                                @if(date("j") == $i)
                                                    <option value="{{$i}}" selected="selected">Ngày {{$i}} </option>
                                                @else
                                                    <option value="{{$i}}">Ngày {{$i}} </option>
                                                @endif
                                            @endfor
                                        </select>
                                        <p class="hide pull-right">Tỉ lệ hiện tại: <span>1 hình ảnh = <?php // echo $response->data->now ?> xu</span></p>
                                    </div>

                                    <!-- <span class="coin_chart" style="height: 160px; padding: 10px 25px;">
                                        <canvas width="100%" height="60" style="display: inline-block; vertical-align: top;"></canvas>
                                    </span> -->
                                    <div id="coin_graph" style="width: 100%;overflow: scroll;position: relative;">Loading graph...</div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_content">

                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div>
                                    <div class="x_title">
                                        <h2 class="caution">Cảnh báo</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        <article class="media event">
                                            <div class="media-body">
                                                <p>1. Nghiêm cần hành vi sử dụng hack/cheat. Nếu phát hiện sẽ block ip vĩnh viễn </p>
                                            </div>
                                        </article>
                                        <article class="media event">
                                            <div class="media-body">
                                                <p>2. Số hình ảnh tối đa làm trong 1 phút là 17 </p>
                                            </div>
                                        </article>
                                        <article class="media event">
                                            <div class="media-body">
                                                <p>3. Không sử dụng 2 ip ( địa chỉ mạng) làm việc cùng một lúc. Điều này sẽ dẫn đến bạn không được nhận tiền</p>
                                            </div>
                                        </article>
                                        <article class="media event">
                                            <div class="media-body">
                                                <p>4. Khi làm xong nên để ít nhất 10p để hệ thống xác nhận toàn bộ ảnh rồi mới tắt</p>
                                            </div>
                                        </article>
                                        <article class="media event">
                                            <div class="media-body">
                                                <p>5. Số coin nhận được sẽ dạo động tùy theo giá thị trường.</p>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="x_title">
                                    <h2>Chia sẻ link giới thiệu</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="" style="overflow:hidden;">
                                    <p><a href="<?php echo $data['urlRef'] ?>"><?php echo $data['urlRef'] ?></a></p>

                                    <p>Chia sẻ link này để nhận được <?php echo $data['parent_percent']*100 ?>% xu từ người giới thiệu làm</p>

                                    <p>Tổng số user giới thiệu: <strong><?php echo $data['total_commission']; ?></strong></p>
                                    <p>Tổng hoa hồng: <strong><?php echo $data['total_coin'];?></strong> xu</p>
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Tên</th>
                                            <th>Ngày tham gia</th>
                                            <th>Coin</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach( $data['userChild'] as $key => $item )
                                        <tr>
                                            <td>{{$item['name']}}</td>
                                            <td>{{$item['created_date']}}</td>
                                            <td>{{$item['coin']}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    // Table.
    jQuery('.table').DataTable({
        "searching":   false,
        "lengthChange": false,
        "info":     false
    });

    function setDay() {
        var year = $('#year option:selected').val();
        var month = $('#month option:selected').val();
        var day = $('#day option:selected').val();
        $('#day option[value="29"]').show();
        $('#day option[value="30"]').show();
        $('#day option[value="31"]').show();

        switch (month) {
            case '1':
            case '3':
            case '5':
            case '7':
            case '8':
            case '10':
            case '12':
                break;
            case '4':
            case '6':
            case '9':
            case '11':
                $('#day option[value="31"]').hide();
                break;
            case '2':
                $('#day option[value="31"]').hide();
                $('#day option[value="30"]').hide();
                if ((year % 4) != 0) {
                    $('#day option[value="29"]').hide();
                }
                break;
        }
    }

    $(document).ready(function() {
        $('#year').on('change', function () {
            setDay();
            drawChart();
        });

        $('#month').on('change', function () {
            setDay();
            drawChart();
        });

        $('#day').on('change', function () {
            drawChart();
        });

        drawChart();

    });

    function drawChart() {
        console.log("drawChart");
        var year = $('#year option:selected').val();
        var month = $('#month option:selected').val();
        var day = $('#day option:selected').val();
        console.log("year: " + year);
        console.log("month: " + month);
        console.log("day: " + day);

        $.ajax({
            url: 'get-rate',
            method: 'post',
            data: {
                'year' : year,
                'month' : month,
                'day' : day,
                _token: $('input[name="_token"]').val()},
            success: function(data){
                var result = JSON.parse(data);
                var coinData = result.data.rate_values;
                var coinChart = new JSChart('coin_graph', 'line');
                coinChart.setDataArray(coinData);
                coinChart.setTitle('');
                coinChart.setAxisNameX('');
                coinChart.setAxisNameY('');
                coinChart.setTooltip([2, 0.123]);
                jQuery.each(result.data.rate_chart, function( index, value ) {
                    coinChart.setTooltip([index, value]);
                });
                coinChart.setSize(31*25, 321);
                coinChart.draw();
            },
            error: function(){
                console.log('error');
            },
        });
    }
</script>
@endsection()