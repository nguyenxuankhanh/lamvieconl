<div class="modal fade" id="checkout-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Thanh toán</h4>
            </div>
            <div class="modal-body">
                <div>
                    <p class="bank-info bank-balance">Số tiền hiện tại :
                        <span><?php // echo @round($realWallet*$exchangeRate,2) ?></span> VND</p>
                    <p class="bank-info bank-available">Số tiền khả dụng : <span><?php // echo @round($realWallet*$exchangeRate,2) ?></span> VND</p>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#mobile" aria-controls="mobile" role="tab" data-toggle="tab">Qua card điện thoại</a></li>
                        <li role="presentation"><a href="#bank" aria-controls="bank" role="tab" data-toggle="tab">Qua ngân hàng</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="mobile">
                            <form id="mobileCheckoutForm" class="form-horizontal" href="#">
                                <div class="form-group">
                                    <label for="type" class="col-sm-2 control-label">Loại thẻ</label>
                                    <?php //
//                                    $data_array = array(
//                                        "token"   => $token
//                                    );
//                                    $data = json_encode($data_array);
//                                    $response = requestApi($apiDomain."getPaymentAccept", $data);
//                                    $response = json_decode($response);
                                    ?>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="card_type" id="card_type">
                                            <?php // foreach ($response->data as $key => $value): ?>
                                            <option value="<?php // echo $key ?>"><?php // echo $value ?></option>
                                            <?php // endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="amount" class="col-sm-2 control-label">Tiền rút</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="amount" id="amount">
                                            <option value="10000">10000</option>
                                            <option value="20000">20000</option>
                                            <option value="50000">50000</option>
                                            <option value="100000">100000</option>
                                            <option value="200000">200000</option>
                                            <option value="300000">300000</option>
                                            <option value="500000">500000</option>
                                        </select>
                                        <!-- <input type="number" name="amount" class="form-control" id="amount" placeholder="10000" required> -->
                                    </div>
                                </div>
                                <p class="caution">*Lưu ý:</p>
                                <p>- Kiểm tra các thông tin nhập thật chính xác, chúng tôi không chịu trách nhiệm nếu bạn nhập sai thông tin </p>
                                <div class="form-group text-center">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-default btn-request-checkout">Gửi yêu cầu thanh toán</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="bank">
                            <form id="bankCheckoutForm" class="form-horizontal" href="#">
                                <div class="form-group">
                                    <label for="bank" class="col-xs-3 col-sm-2 control-label">Ngân hàng</label>
                                    <div class="col-xs-9 col-sm-10">
                                        <input name="bank_name" type="text" class="form-control" id="bank_name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="branch" class="col-xs-3 col-sm-2 control-label">Chi nhánh</label>
                                    <div class="col-xs-9 col-sm-10">
                                        <input name="bank_branch" type="text" class="form-control" id="bank_branch" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="account-number" class="col-xs-3 col-sm-2 control-label">Số tài khoản</label>
                                    <div class="col-xs-9 col-sm-10">
                                        <input name="account_number" type="text" class="form-control" id="account-number" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="account-holder" class="col-xs-3 col-sm-2 control-label">Chủ tài khoản</label>
                                    <div class="col-xs-9 col-sm-10">
                                        <input name="user_name" type="text" class="form-control" id="account-holder" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="amount" class="col-xs-3 col-sm-2 control-label">Số tiền rút</label>
                                    <div class="col-xs-9 col-sm-10">
                                        <input name="amount" type="number" class="form-control" id="bank-amount" placeholder="200000" required>
                                    </div>
                                </div>
                                <p class="caution">*Lưu ý:</p>
                                <p>- Số tiền muốn rút phải từ 200.000 trở lên</p>
                                <p>- Kiểm tra các thông tin nhập thật chính xác, chúng tôi không chịu trách nhiệm nếu bạn nhập sai thông tin </p>
                                <div class="form-group text-center">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-default btn-request-checkout">Gửi yêu cầu thanh toán</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
